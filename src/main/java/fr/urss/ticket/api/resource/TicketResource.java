package fr.urss.ticket.api.resource;

import fr.urss.ticket.domain.Ticket;
import fr.urss.ticket.service.TicketService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

@Path("t")
public class TicketResource {

    @Context
    private UriInfo uriInfo;

    @Inject
    private TicketService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTickets() {
        return Response.ok().build();
    }

    @Path("/tickets")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTickets(String filter) {
        System.out.println("API FIND ALL TICKETS");
        return Response.ok(service.findAll()).build();
    }

    @GET
    @Path("{ticket}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTicketById(@PathParam("ticket") long identifier) {
        try {
            Ticket ticket = service.findTicketById(identifier);
            return Response.ok(ticket).build();
        } catch (NoResultException e) {
            return Response.ok(Response.status(Status.NOT_FOUND)).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createTicket(Ticket ticket) {
        System.out.println("API CREATION DE TICKET");
        return Response.ok(service.create(ticket)).build();
    }

    @PATCH
    @Path("{ticket}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTicket(@PathParam("ticket") long identifier) {
        return Response.ok().build();
    }

    @DELETE
    @Path("{ticket}")
    public Response deleteTicket(@PathParam("ticket") long identifier) {
        return Response.ok().build();
    }

    @PUT
    @Path("/{ticket}/close")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"Technician", "TechnicianManager"})
    public Response closeTicket(long identifier) {
        return Response.ok().build();
    }
}
