package fr.urss.ticket.service;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import fr.urss.company.api.resource.CompanyResource;
import fr.urss.ticket.api.resource.TicketResource;
import fr.urss.user.api.resource.UserResource;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class WsMain {

    public static String url = "http://localhost:8080/ws";

    public static void main(String[] args) {
        JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();

        List<Class<?>> list = new ArrayList<>();
        list.add(UserResource.class);
        list.add(TicketResource.class);
        list.add(CompanyResource.class);

        sf.setResourceClasses(list);
        sf.setProvider(new JacksonJaxbJsonProvider());
        sf.setAddress(url);
        sf.create();

        System.out.println("Server started\nServer is listening : " + url);
        new Scanner(System.in).next();

        System.out.println("Fin");
    }
}
