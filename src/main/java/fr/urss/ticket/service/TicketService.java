package fr.urss.ticket.service;

import fr.urss.ticket.domain.Category;
import fr.urss.ticket.domain.Ticket;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.util.List;

/**
 * Service that provides operations for {@link Ticket}s.
 *
 * @author lucas.david
 */
@ApplicationScoped
public class TicketService {

    @Inject
    private EntityManager manager;


    /**
     * Creates a new {@link Ticket}.
     *
     * @return
     */
    public long create(Ticket ticket) {
        Category cat = createCategory(ticket.getCategory().getName());
        ticket.setCategory(cat);
        try {
            if (!manager.getTransaction().isActive()) manager.getTransaction().begin();
            manager.persist(ticket);
            manager.flush();
            return ticket.getIdentifier();
        } catch (PersistenceException e) {
            manager.getTransaction().rollback();
            throw e; /* or return 0; */
        } finally {
            manager.getTransaction().commit();
        }
    }

    /**
     * Find ticket by Id
     *
     * @param id
     * @return
     * @throws NoResultException
     */
    public Ticket findTicketById(Long id) throws NoResultException {
        Ticket ticket = manager.find(Ticket.class, id);
        if (ticket == null) throw new NoResultException();
        return ticket;
    }

    /**
     * Find all {@link Ticket}s.
     *
     * @return
     */
    public List<Ticket> findAll() {
        return manager.createQuery("SELECT t FROM Ticket t", Ticket.class).getResultList();
    }

    public Long updateTicket(Ticket ticket, Long id) throws NoResultException {
        Ticket t = findTicketById(id);
        //TODO
        //Mettre à jour t en fonction des changements de ticket
        return t.getIdentifier();
    }

    public Category createCategory(String name) {
        Category cat = null;
        try {
            cat = manager.createQuery("SELECT c FROM Category c WHERE c.name = :name", Category.class)
                         .setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            cat = new Category(name);
            try {
                if (!manager.getTransaction().isActive()) manager.getTransaction().begin();
                manager.persist(cat);
                manager.flush();
            } catch (PersistenceException pe) {
                manager.getTransaction().rollback();
                throw e; /* or return 0; */
            }
        }
        return cat;
    }
}
