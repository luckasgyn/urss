package fr.urss.user.api.rmodel;

import fr.urss.common.domain.Address;
import fr.urss.common.domain.Skill;
import fr.urss.company.domain.Company;
import fr.urss.security.domain.Authority;
import fr.urss.user.domain.User;

import java.util.Date;
import java.util.Set;

/**
 * API model for returning user details.
 *
 * @author cassiomolin
 */
public class QueryUser {

    private long identifier;
    private String username;
    private String password;
    private Set<Authority> authorities;
    private boolean active;

    private String firstName;
    private String lastName;
    private String email;
    private Date bornOn;
    private Date hiredOn;
    private String phone;
    private Address address;

    /* Technician Attributes. */
    private Set<Skill> skills;

    /* Customer Attributes. */
    private Company employer;

    public QueryUser(User user, User... users) {
    }


    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBornOn() {
        return bornOn;
    }

    public void setBornOn(Date bornOn) {
        this.bornOn = bornOn;
    }

    public Date getHiredOn() {
        return hiredOn;
    }

    public void setHiredOn(Date hiredOn) {
        this.hiredOn = hiredOn;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Company getEmployer() {
        return employer;
    }

    public void setEmployer(Company employer) {
        this.employer = employer;
    }
}
