package fr.urss.user.domain;

import fr.urss.company.domain.Company;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Customer extends User {

    @JoinColumn(name = "employer")
    @ManyToOne
    private Company employer;

    public Customer() {
    }
}
