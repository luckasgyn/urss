package fr.urss.user.api;


import fr.urss.ArquillianTest;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

/**
 * Tests for the user resource class.
 *
 * @author lucas.david
 */
@RunWith(Arquillian.class)
public class UserResourceTest extends ArquillianTest {

    @Test
    public void getUsersAsAnonymous() {
        var response = client.target(uri).path("api").path("users").request().get();

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void getUsersAsAsUser() { /*
        var authorizationHeader = composeAuthorizationHeader(getTokenForUser());
        var response = client.target(uri).path("api").path("users").request()
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus()); */
    }

    @Test
    public void getUsersAsAdmin() { /*
        var authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());
        var response = client.target(uri).path("api").path("users").request()
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        var queryDetailsList = response.readEntity(new GenericType<List<QueryUserResult>>() {});

        assertNotNull(queryDetailsList);
        assertThat(queryDetailsList, hasSize(3)); */
    }

    @Test
    public void getUserAsAnonymous() {
        long userId = 1L;

        Response response = client.target(uri).path("api").path("users").path(Long.toString(userId)).request().get();
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void getUserAsUser() { /*
        long userId = 1L;

        String authorizationHeader = composeAuthorizationHeader(getTokenForUser());

        Response response = client.target(uri).path("api").path("users").path(Long.toString(userId)).request()
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus()); */
    }

    @Test
    public void getUserAsAdmin() { /*
        long userId = 1L;

        var authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());
        var response = client.target(uri).path("api").path("users").path(Long.toString(userId)).request()
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        var result = response.readEntity(QueryUserResult.class);

        assertNotNull(result);
        assertEquals(userId, result.getId()); */
    }

    @Test
    public void getAuthenticatedUserAsAnonymous() { /*
        var response = client.target(uri).path("api").path("users").path("me").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        var user = response.readEntity(QueryUserResult.class);

        assertNull(user.getId());
        assertEquals("anonymous", user.getUsername());
        assertThat(user.getAuthorities(), is(empty())); */
    }

    @Test
    public void getAuthenticatedUserAsUser() { /*
        var authorizationHeader = composeAuthorizationHeader(getTokenForUser());
        var response = client.target(uri).path("api").path("users").path("me").request()
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        var user = response.readEntity(QueryUserResult.class);

        assertNotNull(user.getId());
        assertEquals("user", user.getUsername());
        assertThat(user.getAuthorities(), containsInAnyOrder(Authority.USER)); */
    }

    @Test
    public void getAuthenticatedUserAsAdmin() { /*
        var authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());
        var response = client.target(uri).path("api").path("users").path("me").request()
                .header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        var user = response.readEntity(QueryUserResult.class);

        assertNotNull(user.getId());
        assertEquals("admin", user.getUsername());
        assertThat(user.getAuthorities(), containsInAnyOrder(Authority.USER, Authority.ADMIN)); */
    }
}
