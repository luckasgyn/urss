package fr.urss;

import org.glassfish.jersey.servlet.ServletContainer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.weld.environment.servlet.Listener;
import org.jboss.weld.environment.servlet.util.Servlets;
import org.junit.Before;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.net.URI;

/**
 * Base Arquillian test class.
 *
 * @author lucas.david
 */
public abstract class ArquillianTest {
    @ArquillianResource
    protected URI uri;

    protected Client client;

    @Deployment(testable = false)
    public static Archive<WebArchive> createDeployment() {
        return null; /* TODO correct this with the servlet container. */
        /* return ShrinkWrap.create(UndertowWebArchive.class).from(
                deployment()
                        .setClassLoader(Application.class.getClassLoader())
                        .setContextPath("/")
                        .addListeners(listener(Listener.class))
                        .addServlets(
                                Servlets.servlet("jerseyServlet", ServletContainer.class)
                                        .setLoadOnStartup(1)
                                        .addInitParam("javax.ws.rs.Application", JerseyConfig.class.getName())
                                        .addMapping("/api/*"))
                        .setDeploymentName("application.war")); */
    }

    @Before
    public void beforeTest() {
        this.client = ClientBuilder.newClient();
    }

    /*
    protected String getTokenForAdmin() {
        var credentials = new Credentials();
        credentials.setUsername("admin");
        credentials.setPassword("password");

        AuthenticationToken authenticationToken = client.target(uri).path("api").path("auth").request()
                .post(Entity.entity(credentials, MediaType.APPLICATION_JSON), AuthenticationToken.class);
        return authenticationToken.getToken();
    } */

    /*
    protected String getTokenForUser() {
        var credentials = new Credentials();
        credentials.setUsername("user");
        credentials.setPassword("password");

        AuthenticationToken authenticationToken = client.target(uri).path("api").path("auth").request()
                .post(Entity.entity(credentials, MediaType.APPLICATION_JSON), AuthenticationToken.class);
        return authenticationToken.getToken();
    } */

    protected String composeAuthorizationHeader(final String authenticationToken) {
        return "Bearer" + " " + authenticationToken;
    }
}
